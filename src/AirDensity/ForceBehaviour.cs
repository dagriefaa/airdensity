﻿using System.Linq;
using UnityEngine;

namespace AirDensity {
    public class ForceBehaviour : MonoBehaviour {

        // aerodynamics
        public Rigidbody Rigid { get; private set; } = null;
        public AxialDrag Axial { get; private set; } = null;
        public FlyingController Spiral { get; private set; } = null;
        public BalloonController Balloon { get; private set; } = null;
        public VacuumBlock Vacuum { get; private set; } = null;
        public FlamethrowerController Flamethrower { get; private set; } = null;
        public BuildSurface Surface { get; private set; } = null;
        public SqrBalloonController SqrBalloon { get; private set; } = null;

        public float RigidDragMagnitude { get; private set; } = 0.05f;
        public Vector3 AxialDragMagnitude { get; private set; } = new Vector3();
        public float SurfaceDragMagnitude { get; private set; } = 1f;
        public float ForceMagnitude { get; private set; } = 1f;
        public float BalloonMagnitude { get; private set; } = 1f;
        public float VacuumPower { get; private set; } = 1f;
        public float SqrBalloonBuoyancy { get; private set; } = 1f;
        public float FlamethrowerDrag { get; private set; } = 0.6f;
        public float FlamethrowerInherit { get; private set; } = 0.6f;
        public float AirDensity { get; private set; } = 1f;

        // orbitals
        float Mu { get { return SphereOfInfluence ? SphereOfInfluence.Mass : 1f; } }
        public Vector3 LocalPosition {
            get {
                return transform.position - (SphereOfInfluence
                            ? SphereOfInfluence.transform.position
                            : Vector3.zero);
            }
        }
        Vector3 LocalVelocity {
            get {
                return Rigid.velocity - ((SphereOfInfluence?.rigidbody)
                            ? SphereOfInfluence.rigidbody.velocity
                            : Vector3.zero);
            }
        }
        Vector3 AngularMomentum { get { return Vector3.Cross(LocalPosition, LocalVelocity); } }
        Vector3 NodeVector { get { return Vector3.Cross(Vector3.up, AngularMomentum); } }
        float SpecificEnergy { get { return LocalVelocity.sqrMagnitude / 2f - Mu / LocalPosition.magnitude; } }
        Vector3 EccentricityVector {
            get {
                return ((LocalVelocity.sqrMagnitude - Mu / LocalPosition.magnitude) * LocalPosition
                    - Vector3.Dot(LocalPosition, LocalVelocity) * LocalVelocity) / Mu;
            }
        }
        float EccentricAnomaly { get { return 2f * Mathf.Atan(Mathf.Tan(TrueAnomaly / 2f) / Mathf.Sqrt((1f + Eccentricity) / (1f - Eccentricity))); } }
        float MeanAnomaly { get { return EccentricAnomaly - Eccentricity * Mathf.Sin(EccentricAnomaly); } }

        public float LocalAltitude { get { return SphereOfInfluence ? LocalPosition.magnitude : gameObject.transform.position.y; } }
        public float LocalSpeed { get { return SphereOfInfluence ? LocalVelocity.magnitude : Rigid.velocity.magnitude; } }
        public float PeriapsisAltitude { get { return SemiMajorAxis * (1f - Eccentricity); } }
        public float ApoapsisAltitude { get { return SemiMajorAxis * (1f + Eccentricity); } }
        public float TimeToPeriapsis { get { return (2f * Mathf.PI - MeanAnomaly) * Mathf.Sqrt(SemiMajorAxis * SemiMajorAxis * SemiMajorAxis / Mu); } }
        public float TimeToApoapsis { get { return (Mathf.PI - MeanAnomaly) * Mathf.Sqrt(SemiMajorAxis * SemiMajorAxis * SemiMajorAxis / Mu); } }
        public float Eccentricity { get { return SphereOfInfluence ? EccentricityVector.magnitude : 0f; } }
        public float SemiMajorAxis {
            get {
                return SphereOfInfluence
                        ? EccentricityVector.sqrMagnitude < 1f
                        ? -(Mu / (2 * SpecificEnergy))
                        : Mathf.Infinity
                    : 0f;
            }
        }
        public float Inclination { get { return SphereOfInfluence ? Mathf.Acos(AngularMomentum.y / AngularMomentum.magnitude) : 0f; } }
        public float LongitudeAscending { get { return SphereOfInfluence ? Mathf.Acos(AngularMomentum.x / AngularMomentum.magnitude) : 0f; } }
        public float ArgOfPeriapsis {
            get {
                return SphereOfInfluence ? Mathf.Acos(Vector3.Dot(NodeVector, EccentricityVector) / NodeVector.magnitude * Eccentricity) : 0f;
            }
        }
        public float TrueAnomaly {
            get {
                float anomaly = SphereOfInfluence ? Mathf.Acos(Vector3.Dot(EccentricityVector, LocalPosition) / (Eccentricity * LocalPosition.magnitude)) : 0f;
                return Vector3.Dot(LocalPosition, LocalVelocity) < 0 ? 2f * Mathf.PI - anomaly : anomaly;
            }
        }

        GravityWellBehaviour thisGravityWell = null;
        public GravityWellBehaviour SphereOfInfluence { get; private set; } = null;

        // Use this for initialization
        void Start() {
            Rigid = GetComponent<Rigidbody>();
            Axial = GetComponent<AxialDrag>();
            Spiral = GetComponent<FlyingController>();
            Balloon = GetComponent<BalloonController>();
            Vacuum = GetComponent<VacuumBlock>();
            Flamethrower = GetComponent<FlamethrowerController>();
            SqrBalloon = GetComponent<SqrBalloonController>();

            if (Rigid)
                RigidDragMagnitude = Rigid.drag;
            if (Axial)
                AxialDragMagnitude = Axial.AxisDrag;
            if (Spiral) {
                ForceMagnitude = Spiral.SpeedSlider.Value;
                Spiral.speed *= ForceMagnitude;
            }
            if (Balloon)
                BalloonMagnitude = Balloon.BuoyancySlider.Value;
            if (Vacuum)
                VacuumPower = Vacuum.PowerSlider.Value;
            if (Surface && Surface.aero.IsActive) {
                SurfaceDragMagnitude = Surface.currentType.dragMultiplier;
            }
            if (SqrBalloon) {
                SqrBalloonBuoyancy = SqrBalloon.PowerSlider.Value;
            }
            if (Flamethrower) {
                FlamethrowerDrag = Flamethrower.fireParticles.limitVelocityOverLifetime.dampen;
                FlamethrowerInherit = Flamethrower.fireParticles.inheritVelocity.curve.constant;
            }

            foreach (Transform t in transform) {
                t.gameObject.AddComponent<ForceBehaviour>();
            }

            if (!(Rigid || Axial || Spiral || Balloon || Vacuum || Flamethrower || (Surface && Surface.aero.IsActive) || Balloon)) {
                Destroy(this);
                return;
            }

            thisGravityWell = GetComponent<GravityWellBehaviour>();
        }

        void FixedUpdate() {
            Air(AirDensity = Density(LocalAltitude, SphereOfInfluence));

            if (Rigid && thisGravityWell) {
                float minDistance = Mathf.Infinity;
                foreach (GravityWellBehaviour gravityWell in AirDensityController.GravityWells) {
                    if (gravityWell == thisGravityWell)
                        continue;
                    float sqrDistance = Vector3.SqrMagnitude(gravityWell.transform.position - transform.position);
                    float influence = gravityWell.InfluenceRadius;
                    if (sqrDistance < influence * influence) {
                        Rigid.AddForce(gravityWell.Gravity(gameObject), ForceMode.Acceleration);
                        if (sqrDistance < minDistance) {
                            minDistance = sqrDistance;
                            SphereOfInfluence = gravityWell;
                        }
                    }
                }
            }
        }

        public static float Density(float altitude, GravityWellBehaviour soi = null) {
            float seaLevel = soi ? soi.seaLevelAltitude : AirDensityController.SeaLevelAltitude;
            float atmoEnd = soi ? soi.atmosphereEndAltitude : AirDensityController.AtmosphereEndAltitude;

            return Mathf.Exp(-(3 * (altitude - seaLevel)) / (atmoEnd - seaLevel));
        }

        void Air(float density) {
            if (AirDensityController.DensityPreset == AirDensityController.Density.Disabled) {
                if (Spiral && !Spiral.RegisteredSimFixedUpdate && Spiral.flying) {
                    Spiral.ParentMachine.RegisterFixedUpdate(Spiral, false);
                }
                return;
            }
            if (Rigid) {
                Rigid.drag = RigidDragMagnitude * density;
            }
            if (Axial)
                Axial.AxisDrag = AxialDragMagnitude * density;
            if (Balloon)
                Balloon.BuoyancySlider.Value = BalloonMagnitude * density;
            if (Vacuum)
                Vacuum.powerSlider.Value = VacuumPower * density;
            if (Flamethrower) {
                ParticleSystem.LimitVelocityOverLifetimeModule dmod = Flamethrower.fireParticles.limitVelocityOverLifetime;
                dmod.dampen = FlamethrowerDrag * density;
                ParticleSystem.InheritVelocityModule imod = Flamethrower.fireParticles.inheritVelocity;
                imod.curve = new ParticleSystem.MinMaxCurve(1f - (1f - FlamethrowerInherit) * density);
            }
            if (Spiral) {
                Spiral.SpeedSlider.Value = ForceMagnitude * density;

                if (Spiral && Spiral.RegisteredSimFixedUpdate) {
                    Spiral.ParentMachine.UnregisterFixedUpdate(Spiral, false);
                }

                if (!Spiral.canFly || !Spiral._parentMachine.isReady) {
                    return;
                }
                if (Spiral.speedToGo != Vector3.zero) {
                    float magnitude = ForceMagnitude * 100f * (!Spiral.ReverseToggle.IsActive ? 1f : -1f) * density;
                    Spiral.Rigidbody.AddForce(Spiral.transform.forward * magnitude);
                    Rigid.drag = 1.5f * density;
                }
            }
            if (Surface && Surface.aero.IsActive) {
                Surface.currentType.dragMultiplier = SurfaceDragMagnitude * density;
            }
            if (SqrBalloon) {
                SqrBalloon.PowerSlider.Value = SqrBalloonBuoyancy * density;
            }
        }
    }
}