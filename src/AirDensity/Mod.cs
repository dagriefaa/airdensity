using Modding;
using ObjectExplorer.Mappings;
using System;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace AirDensity {
    public class Mod : ModEntryPoint {
        public override void OnLoad() {
            Events.OnBlockInit += x => {
                if (!ReferenceMaster.activeMachineSimulating) {
                    return;
                }
                x.GameObject.AddComponent<ForceBehaviour>();
                if (x.InternalObject is FlamethrowerController && !x.InternalObject.gameObject.GetComponent<FlamethrowerLight>()) {
                    x.InternalObject.gameObject.AddComponent<FlamethrowerLight>();
                }
            };

            GameObject controller = GameObject.Find("ModControllerObject");
            if (!controller) UnityEngine.Object.DontDestroyOnLoad(controller = new GameObject("ModControllerObject"));

            GameObject root = new GameObject("AirDensityController");
            root.transform.parent = controller.transform;
            root.AddComponent<AirDensityController>();

            if (Mods.IsModLoaded(new Guid("565fdc41-d8bc-452c-81f8-09b16934f618"))) {
                controller.AddComponent<InstrumentalityInterface>();
            }
            AddMappings();
        }

        static void AddMappings() {

            ObjectExplorer.ObjectExplorer.AddMappings("AirDensity",
                new MEnum<AirDensityController, AirDensityController.Density>("DensityPreset",
                    c => AirDensityController.DensityPreset,
                    (c, x) => AirDensityController.SetDensityPreset(x)
                ),
                new MFloat<AirDensityController>("MinDensityAltitude",
                    c => AirDensityController.AtmosphereEndAltitude,
                    (c, x) => {
                        AirDensityController.AtmosphereEndAltitude = x;
                        AirDensityController.SetDensityPreset(AirDensityController.Density.Custom);
                    }
                ),
                new MFloat<AirDensityController>("MaxDensityAltitude",
                    c => AirDensityController.SeaLevelAltitude,
                    (c, x) => {
                        AirDensityController.SeaLevelAltitude = x;
                        AirDensityController.SetDensityPreset(AirDensityController.Density.Custom);
                    }
                ),
                new MButton<AirDensityController>("MakeTestRange",
                    (c) => Mod.MakeTestRange(),
                    c => SceneManager.GetActiveScene().name == "BARREN EXPANSE"
                ),

                //new MFloat<AirDensityController>("LocalSpeed", c => AirDensityController.Machine?.LocalSpeed),
                //new MFloat<AirDensityController>("LocalAltitude", c => AirDensityController.Machine?.LocalAltitude),
                //new MFloat<AirDensityController>("AirDensity", c => AirDensityController.Machine?.AirDensity),
                //new MFloat<AirDensityController>("PeriapsisAltitude", c => AirDensityController.Machine?.PeriapsisAltitude),
                //new MFloat<AirDensityController>("ApoapsisAltitude", c => AirDensityController.Machine?.ApoapsisAltitude),
                //new MFloat<AirDensityController>("TimeToPeriapsis", c => AirDensityController.Machine?.TimeToPeriapsis),
                //new MFloat<AirDensityController>("TimeToApoapsis", c => AirDensityController.Machine?.TimeToApoapsis),
                //new MFloat<AirDensityController>("Eccentricity", c => AirDensityController.Machine?.Eccentricity),
                //new MFloat<AirDensityController>("SemiMajorAxis", c => AirDensityController.Machine?.SemiMajorAxis),
                //new MFloat<AirDensityController>("Inclination", c => AirDensityController.Machine?.Inclination * Mathf.Rad2Deg),
                //new MFloat<AirDensityController>("LongitudeAscending", c => AirDensityController.Machine?.LongitudeAscending * Mathf.Rad2Deg),
                //new MFloat<AirDensityController>("PericenterArgs", c => AirDensityController.Machine?.ArgOfPeriapsis * Mathf.Rad2Deg),
                //new MFloat<AirDensityController>("TrueAnomaly", c => AirDensityController.Machine?.TrueAnomaly * Mathf.Rad2Deg),

                new MFloat<ForceBehaviour>("airDensity", c => c.AirDensity),
                new MVector3<ForceBehaviour>("axialDragMagnitude", c => c.AxialDragMagnitude, isVisible: c => c.Axial),
                new MFloat<ForceBehaviour>("balloonMagnitude", c => c.BalloonMagnitude, isVisible: c => c.Balloon),
                new MFloat<ForceBehaviour>("forceMagnitude", c => c.ForceMagnitude, isVisible: c => c.Spiral),
                new MFloat<ForceBehaviour>("rigidDragMagnitude", c => c.RigidDragMagnitude, isVisible: c => c.Rigid),
                new MComponent<ForceBehaviour, AxialDrag>("axialDrag", c => c.Axial, isVisible: c => c.Axial),
                new MComponent<ForceBehaviour, BalloonController>("balloonBlock", c => c.Balloon, isVisible: c => c.Balloon),
                new MComponent<ForceBehaviour, FlyingController>("flyingBlock", c => c.Spiral, isVisible: c => c.Spiral),
                new MComponent<ForceBehaviour, Rigidbody>("rigidbody", c => c.Rigid, isVisible: c => c.Rigid),

                new MFloat<GravityWellBehaviour>("Acceleration", c => c.acceleration),
                new MFloat<GravityWellBehaviour>("Radius", c => c.radius),
                new MFloat<GravityWellBehaviour>("MinDensityAltitude", c => c.atmosphereEndAltitude),
                new MFloat<GravityWellBehaviour>("MaxDensityAltitude", c => c.seaLevelAltitude)
            );

            //if (ObjectExplorer.Storage.ContainsKey("AirDensity")) {
            //    try {
            //        GravityWellBehaviour.defaultRadius = ((float)ObjectExplorer.Storage["AirDensity"][0]);
            //        ((List<object[]>)ObjectExplorer.Storage["AirDensity"][1]).ForEach(
            //            x => GravityWellBehaviour.MakeGravityWell((GameObject)x[0], (float)x[1], (bool)x[2], (float)x[3], (float)x[4])
            //            );
            //    } catch (Exception e) {
            //        Debug.LogError("[AirDensity] Storage not formatted correctly!");
            //        Debug.LogError(e);
            //    }
            //}

            ObjectExplorer.ObjectExplorer.AddShortcut(() => AirDensityController.Instance.gameObject);

        }

        static void MakeTestRange() {
            if (SceneManager.GetActiveScene().name == "BARREN EXPANSE") {
                GameObject floor = GameObject.Find("FloorBig");
                floor.transform.localScale = new Vector3(25000, 10, 25000);
                floor = UnityEngine.Object.Instantiate(floor, floor.transform.parent) as GameObject;
                floor.transform.position += new Vector3(0, 0, 25000f);
                floor = UnityEngine.Object.Instantiate(floor, floor.transform.parent) as GameObject;
                floor.transform.position += new Vector3(0, 0, 25000f);
            }
            Camera.main.nearClipPlane = 3f;
            Camera.main.farClipPlane = 30000f;
            AirDensityController.SetDensityPreset(AirDensityController.Density.Custom);
            AirDensityController.AtmosphereEndAltitude = 2500f;
            AirDensityController.SeaLevelAltitude = 0f;
            MouseOrbit.maxZoom = 3350;
            //Action<Block> engineFlame = b => {
            //    if (b.InternalObject is FlamethrowerController) {
            //        ParticleSystem sys = b.InternalObject.transform.FindChild("Fire").GetComponent<ParticleSystem>();
            //        ParticleSystem.ColorOverLifetimeModule module = sys.colorOverLifetime;
            //        ParticleSystem.MinMaxGradient color = module.color;
            //        Gradient grad = new Gradient();
            //        grad.colorKeys = new GradientColorKey[] {
            //            new GradientColorKey(Color.blue, 0),
            //            new GradientColorKey(new Color(1, 0.6f, 0.6f), 0.2f),
            //            new GradientColorKey(new Color(1, 0.64f, 0), 1)
            //        };
            //        grad.alphaKeys = color.gradient.alphaKeys;
            //        color.gradient = grad;
            //    }
            //};
            //Events.OnBlockInit -= engineFlame;
            //Events.OnBlockInit += engineFlame;
        }
    }
}
