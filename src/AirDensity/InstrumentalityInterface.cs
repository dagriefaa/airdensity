﻿using Instrumentality.SimDisplay;
using UnityEngine;

namespace AirDensity {
    public class InstrumentalityInterface : MonoBehaviour {

        Rigidbody targetRigidbody = null;
        ForceBehaviour target = null;

        void Awake() {
            SimDisplayController.AltitudeFunction = x => {
                return (x.gameObject == targetRigidbody) ? target.LocalAltitude : x.position.y;
            };
            SimDisplayController.AirDensityFunction = x => {
                if (AirDensityController.DensityPreset == AirDensityController.Density.Disabled) {
                    return 1f - (SimDisplayController.AltitudeFunction(x) / 2000f);
                }
                return ForceBehaviour.Density(SimDisplayController.AltitudeFunction(x),
                    (x.gameObject == targetRigidbody) ? target.SphereOfInfluence : null);
            };
        }

        void Update() {
            if (!targetRigidbody) {
                return;
            }
            if (targetRigidbody != SimDisplayController.TargetRigidbody) {
                targetRigidbody = SimDisplayController.TargetRigidbody;
                target = targetRigidbody.GetComponent<ForceBehaviour>();
            }
        }
    }

}
