﻿using UnityEngine;
using System.Collections.Generic;

namespace AirDensity {
    public class AirDensityController : MonoBehaviour {


        public static List<GravityWellBehaviour> GravityWells { get; private set; } = new List<GravityWellBehaviour>();
        public static GravityWellBehaviour Center;
        public static AirDensityController Instance;

        public enum Density {
            Disabled, Normal, Low, None, Custom
        }

        public static Density DensityPreset { get; private set; } = Density.Disabled;

        private static float _atmosphereEndAltitude = 2000f;
        public static float AtmosphereEndAltitude {
            get { return _atmosphereEndAltitude; }
            set {
                if (value < SeaLevelAltitude)
                    return;
                _atmosphereEndAltitude = value;
            }
        }
        private static float _seaLevelAltitude = 0f;
        public static float SeaLevelAltitude {
            get { return _seaLevelAltitude; }
            set {
                if (value > AtmosphereEndAltitude)
                    return;
                _seaLevelAltitude = value;
            }
        }

        void Start() {
            DontDestroyOnLoad(gameObject);
            Instance = this;
        }

        public static void SetDensityPreset(Density d) {
            DensityPreset = d;
            switch (d) {
                case Density.Disabled:
                    break;
                case Density.Normal:
                    AtmosphereEndAltitude = 2000;
                    SeaLevelAltitude = 0;
                    break;
                case Density.Low:
                    AtmosphereEndAltitude = 1500;
                    SeaLevelAltitude = -500;
                    break;
                case Density.None:
                    SeaLevelAltitude = -1E12f;
                    AtmosphereEndAltitude = -1E12f;
                    break;
                case Density.Custom:
                    break;

            }
        }
    }
}