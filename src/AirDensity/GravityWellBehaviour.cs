﻿using UnityEngine;
using System.Collections;

namespace AirDensity {
    public class GravityWellBehaviour : MonoBehaviour {

        public float radius = 10f; // planet radius or max gravity
        public float acceleration = Physics.gravity.magnitude; // gravity force

        public float seaLevelAltitude;
        public float atmosphereEndAltitude;

        /* some notes:
         * gravity strength: F = (G * M1)/r^2
         * 
         * F = (G * M1)/r^2
         * M1 = F * r^2 / G
         * 
         * let F = a and r = c, where a = desired acceleration and c = desired altitude
         * 
         * M1 = a * c^2 / G
         * 
         * thus
         * F = (G * a * c^2 / G) / r^2
         * F = (a * c^2) / r^2
         * 
         * orbital velocity: sqrt((G * M)/r)
         * or: sqrt((a * c^2)/r)
         * 
         * escape velocity: sqrt((2 * G * M)/r)
         * or: sqrt((2 * a * c^2)/r)
         * 
        */

        public float Mass { get { return acceleration * radius * radius; } }
        public Rigidbody rigidbody { get; private set; }
        public ForceBehaviour forceBehaviour { get; private set; }
        public float InfluenceRadius {
            get {
                return forceBehaviour && rigidbody
                    ? forceBehaviour.SemiMajorAxis * Mathf.Pow(Mass / AirDensityController.GravityWells[0].Mass, 0.4f)
                    : Mathf.Infinity;
            }
        }

        // Use this for initialization
        void Start() {
            AirDensityController.GravityWells.Add(this);
            rigidbody = GetComponent<Rigidbody>();
        }

        void OnDestroy() {
            AirDensityController.GravityWells.Remove(this);
        }

        void OnEnable() {
            AirDensityController.SetDensityPreset(AirDensityController.Density.None);
        }

        void OnDisable() {
            AirDensityController.SetDensityPreset(AirDensityController.Density.Disabled);
        }

        public Vector3 Gravity(GameObject target) {
            if (!(enabled && gameObject.activeSelf && gameObject.activeInHierarchy))
                return Vector3.zero;
            Vector3 toThis = gameObject.transform.position - target.gameObject.transform.position;
            float sqrMagnitude = toThis.sqrMagnitude;
            toThis.Normalize();
            if (sqrMagnitude < radius * radius)
                return toThis * acceleration;

            return acceleration * (radius * radius) / sqrMagnitude * toThis;
        }

        public static void MakeGravityWell(GameObject o, float radius, bool isMoon, float atmoFalloffStartRadius, float atmoEndAltitude) {
            GravityWellBehaviour gw = o.AddComponent<GravityWellBehaviour>();
            gw.radius = radius;
            gw.seaLevelAltitude = atmoFalloffStartRadius;
            gw.atmosphereEndAltitude = atmoFalloffStartRadius + atmoEndAltitude;
            if (!AirDensityController.Center)
                AirDensityController.Center = gw;
            if (isMoon) {
                gw.acceleration *= gw.radius / defaultRadius;
                Rigidbody r = o.GetComponent<Rigidbody>();
                r.isKinematic = false;
                Vector3 planetMotion = o.transform.position - AirDensityController.Center.transform.position;
                Debug.Log($"[AirDensity] Gravity well: {o.name} with " +
                    $"radius {gw.radius}, " +
                    $"distance {planetMotion.magnitude}, " +
                    $"inclination {Vector3.Angle(planetMotion, new Vector3(planetMotion.x, 0f, planetMotion.z))}");
                planetMotion = Quaternion.Euler(0f, 90f, 0f) * planetMotion;
                float orbitalSpeed = Mathf.Sqrt(
                    AirDensityController.Center.acceleration
                    * AirDensityController.Center.radius
                    * AirDensityController.Center.radius
                    / planetMotion.magnitude);
                r.velocity = planetMotion.normalized * orbitalSpeed;
                r.transform.localEulerAngles = planetMotion;
                gw.forceBehaviour = o.AddComponent<ForceBehaviour>();
                r.mass = gw.acceleration * (gw.radius * gw.radius) / 0.01f;
            }
        }

        public static float defaultRadius = 1500f;
    }
}