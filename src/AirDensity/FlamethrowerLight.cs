﻿using UnityEngine;
using System.Collections;

public class FlamethrowerLight : MonoBehaviour {

    FlamethrowerController fm;
    Light spot;
    Light point;
    
    void Start() {
        fm = GetComponent<FlamethrowerController>();
        Transform fire = (fm.RangeSlider.Value >= 0) ? GetComponentInChildren<ParticleSystem>().transform : fm.transform;

        GameObject lightObject = new GameObject("PointLight");
        lightObject.transform.SetParent(fire.transform, false);
        lightObject.transform.localRotation = (fm.RangeSlider.Value >= 0) ? Quaternion.identity : Quaternion.Euler(0f, 180f, 0f);
        point = lightObject.gameObject.AddComponent<Light>();
        point.color = new Color(1f, 0.3f, 0f);
        point.intensity = 3f;
        point.shadows = LightShadows.None;
        point.range = 5;

        GameObject spotObject = new GameObject("Spotlight");
        spotObject.transform.SetParent(lightObject.transform, false);
        spot = spotObject.gameObject.AddComponent<Light>();
        spot.color = new Color(1f, 0.3f, 0f);
        spot.intensity = 3f;
        spot.shadows = LightShadows.None;
        spot.type = LightType.Spot;
        spot.spotAngle = 120;

        point.enabled = false;
        spot.enabled = false;
    }

    void Update() {
        if (Time.timeScale < 0.01f) return;

        point.enabled = fm.isFlaming;
        spot.enabled = fm.isFlaming;
        spot.range = (Mathf.Abs(fm.RangeSlider.Value) <= 100) ? 2 + Mathf.Abs(fm.RangeSlider.Value) : 0f;
    }
}
