# Air Density
https://steamcommunity.com/sharedfiles/filedetails/?id=1747633774

Air Density is a mod for [Besiege](https://store.steampowered.com/app/346010) which makes atmospheric drag altitude-based, and also contains modules for planetary gravity. Object Explorer is required for configuration.

## Technologies
* Unity 5.4 - the game engine Besiege runs on
* C# 3.0 - the scripting language that Unity 5.4 shipped with

## Installation
The latest version of the mod can be found on the Steam Workshop, and can be installed at the press of the big green 'Subscribe' button. It will automatically activate the next time you boot the game.

If, for whatever reason, you don't want to use Steam to manage your mods, installation is as follows:
1. Install Besiege.
2. Clone the repository into `Besiege/Besiege_Data/Mods`.
3. Open the `.sln` file in `src` in Visual Studio 2015 (or later).
    - A bug means that VS versions after 2019 can only be used if VS2019 or earlier is installed with the `.NET Framework 3.5 development tools` component.
4. Press F6 to compile the mod.

## Usage
Once installed, open Object Explorer and open the `AirDensityController` shortcut.

Air pressure decays with altitude according to `e ^ (-3 * (altitude - MaxDensityAltitude) / (MinDensityAltitude - MaxDensityAltitude))`.

There are three `DensityPreset` modes which can be cycled with `SetDensityPreset`:
1. Disabled (no effect)
2. Normal (100% pressure at 0m, 10% pressure at 2000m)
3. Low (100% pressure at -500m, 10% pressure at 1500m)

`MaxDensityAltitude` and `MinDensityAltitude` can be set directly as well.

